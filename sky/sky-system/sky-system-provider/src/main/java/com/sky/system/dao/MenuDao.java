package com.sky.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.system.api.model.Menu;

/**
 * @date 2020-12-16 016 11:37
 */
public interface MenuDao extends BaseMapper<Menu> {
}
