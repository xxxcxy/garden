package com.sky.system.dao;

import com.sky.framework.dao.mapper.MyBaseMapper;
import com.sky.system.api.model.Dictionary;

/**
 * @date 2020-11-03 003 10:00
 */
public interface DictionaryDao extends MyBaseMapper<Dictionary> {
}
