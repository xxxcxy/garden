package com.sky.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.file.api.model.FileInfo;

/**
 * @date 2020-12-14 014 19:58
 */
public interface FileInfoDao extends BaseMapper<FileInfo> {
}
